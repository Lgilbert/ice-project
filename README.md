This project is adressed to Emmanuelle Becker for individual evaluation of the 
ICE UE.

Inside is the actual Jupyter code file for the analysis on english words in the
human proteome.

I added the list of words used for it, as well as a subset of the human proteome
fasta file (the full one is too large). Those can be used to test the code in a 
another software or on one's computer offline. 

Have fun with it!

P.S: the "forage" script file is the group project on dynamic optimization. 